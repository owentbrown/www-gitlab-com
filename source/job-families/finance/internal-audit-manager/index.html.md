---
layout: job_family_page
title: "Internal Audit"
---
## Internal Audit
The Internal Audit Role drives the reporting package to the Audit Committee.

## Levels

### Internal Audit, Manager

#### Job Grade
The Internal Audit, Manager is a [9](/handbook/total-rewards/compensation/#gitlab-job-grades).

#### Responsibilities

* Primary responsibility for determining, documenting and implementing the Company’s internal audit policies.
* Document financial processes and perform test of controls for SOX compliance and audits.
* Assist with the execution of the internal audit plan and timely completion of assigned audits.
* Assist with enterprise risk management activities.
* Recommend improvements related to the Company’s key controls.
* Work closely with external auditors.
* Must be able to work collaboratively with the operational accounting team and business functions.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Act as a subject matter expert, working with the business partners in accounting and other functions (e.g., legal, corporate development, stock administration) to identify financial risks associated with new or contemplated transactions and resolve complex accounting issues.
* Participate in team planning including setting team goals and priorities, monitoring progress and removing roadblocks.

## Requirements

* Proven work experience in Internal Auditing or similar role.
* Must have at least 5 years experience in a big 4 public accounting firm.
* Certified Public Accountant desirable.
* Strong technical, analytic, and communication skills (both written and verbal).
* In-depth knowledge of SEC filing requirements, experience highly preferred.
* Must have direct knowledge of US GAAP.
* Must have public company experience with Sarbanes Oxley.
* Strong working knowledge of GAAP principles and financial statements.
* Proficient with google sheets and GSuite.
* Revenue recognition, namely 606, experience highly preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision.
* Ability to balance quality of work with speed of execution.
* Ability to use GitLab.

### Director, Internal Audit

## Job Grade

The Director, Internal Audit is a [10](/handbook/total-rewards/compensation/#gitlab-job-grades).

#### Responsibilities

All the above responsibilities, plus
* Drives the reporting package to the Audit Committee.
* Documents GitLab SOX controls, processes, and recommends additional controls where there are control deficiencies.
* Is the subject matter expert on controls with GitLab business partners, audit committee, and C-Suite executives.
* Build and manage a highly functioning, distributed team of direct reports.
* Manages an intern program to bring on new personnel to train and to fit into the accounting/finance functions.
* Works with the Legal and Technical Accounting teams to identify related party companies from inquiries from the Board and C-Suite team.

#### Requirements
In addition to the above, the requirements for the Director, Internal Audit must
* Previous management experience; ability to contribute to the career development of staff and a culture of teamwork.

## Performance Indicators
* [Percentage of Desktop procedures documented](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of controls tested](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of recommendations implemented](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of audits completed](/handbook/internal-audit/#internal-audit-performance-measures)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
