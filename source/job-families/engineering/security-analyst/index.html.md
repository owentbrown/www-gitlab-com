---
layout: job_family_page
title: "Security Analyst"
---

As a member of the Security team at GitLab, you will be working towards raising the bar on security for GitLab the company, GitLab the product, and GitLab.com. We will achieve that by working and collaborating with cross-functional teams to provide guidance on security best practices, implementing security improvements, and reacting to security events and incidents.

The [Security Team](/handbook/engineering/security) is responsible for leading and implementing the various initiatives that relate to improving GitLab's security, as well as responding to any security incidents that may arise.

## Analyst Requirements

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values
- Ability to use GitLab

## Field Security

### Security Analyst

Responsibilities:

- Develop security training and guidance to internal development teams
- Assist with recruiting activities and administrative work
- Communication
  * Handle communications with independent vulnerability researchers.
  * Educate other developers on workflows and processes.
  * Ability to professionally handle communications with outside researchers, users, and customers.
  * Ability to communicate clearly on related security issues.


### Senior Security Analyst

Responsibilities:

* Leverages security expertise in at least one specialty area
* Triages and handles/escalates security issues independently
* Conduct reviews and makes recommendations
* Great written and verbal communication skills
* Screen security candidates during hiring process

## Anti-Abuse

### Senior Security Analyst

The Anti-Abuse Analyst is responsible for leading and implementing the various initiatives that relate to improving GitLab's security including:

- Handle tickets/requests escalated to abuse
- Handle DMCA, phishing, malware, botnet, intrusion attempts, DoS, port scanning, spam, spam website, PII and web-crawling abuse reports to point of mitigation of abuse
- Verify proper classification of incoming abuse reports
- Execute messaging to customers on best practices
- Monitoring email, forums, and other communication channels for abuse, and responding accordingly
- Assist with recruiting activities and administrative work
- Making sure internal knowledge reference pages are updated
- Handle communications with independent vulnerability researchers and triage reported abuse cases.
- Educate other developers on anti-abuse cases, workflows and processes.
- Ability to professionally handle communications with outside researchers, users, and customers.
- Ability to communicate clearly on anti-abuse issues.

## Security Compliance

Security Compliance Analysts focus on defining and shaping GitLab’s security compliance programs and are experts in all things security compliance. We are looking for people who are comfortable building transparent compliance programs and who understand how compliance works with cloud-native technology stacks.

### Security Analyst

#### Responsibilities


- Conduct security reviews and makes recommendations
- Screen security candidates during hiring process
- Coordinate work of internal and external auditors or advisors as needed
- Educate others on compliance workflows and processes
- Professionally handle communications with internal and external stakeholders
- Communicate clearly on compliance issues

#### Requirements
- Proven experience operating compliance programs
- Demonstrated experience with at least two security control frameworks (e.g. SOC2, ISO, NIST, PCI, etc.)
- Passion for transparent compliance programs
- Working understanding of how compliance works with cloud-native technology stacks
- Passion for security and open source
- Team player, and enjoy collaborating with cross-functional teams
- Strong communication skills
- Proven ability to employ a flexible and constructive approach when solving problems
- You share our values, and work in accordance with those values

#### Compa Groups

General definitions for compa groups can be found on the [GitLab Global Compensation handbook page](/handbook/total-rewards/compensation/#compa-ratio). Below is a more targeted definition of what it means to be in a compa group within each level of security compliance analyst roles.

Within the responsibilities and requirements for this level, the differentiation between compa groups is:
- Learning: New to this level with regular input and guidance needed to accomplish responsibilities.
- Growing:  The ability to operate independently on the vast majority of day-to-day tasks with more guidance needed on new programs and initiatives.
- Thriving: The ability to operate independently on the programatic level with strong cross-team communication. This is the compa group that most security analysts will spend the most time in within a particular role. This compa group involves regularly operating outside of your comfort zone, continually seeking out new work, and helping to distribute work across the team to ensure team-level OKR's are met. Individual OKR expectations are regularly met.
- Expert: Guidance on tasks and projects is rarely needed and there is very little left to learn within the responsibilities and requirements of this role. All work is performed transparently and with a sense of urgency. OKR expectations are regularly exceeded and data exists to support expertise in all areas of responsibilities and requirements.

### Senior Security Analyst

#### Responsibilities
- All responsibilities for a Security Analyst; plus:
- Develop roadmap based on customer needs e.g.: GDPR, SOC 2, FIPS 140-2
- Align other security specialist activities with the compliance roadmap
- Manage external audits and coordinate audit evidence
- Build security controls that map to GitLab security compliance requirements
- Operate existing controls and collect audit evidence with minimal guidance
- Break down complex problems into specific next steps
- Define compliance requirements that do not adversely impact GitLab teams yet still solve underlying compliance needs

#### Requirements
- All requirements for a Security Analyst; plus:
- Proven experience defining and shaping compliance programs
- Excellent communication skills
- Ability to build simple solutions to complex problems
- Strong collaboration across teams outside of Security
- Proven ability to iterate and improve on existing processes and programs
- Ability to build consensus without formal authority
- Ability to operate effectively in ambiguity
- Strong knowledge in most GitLab tools, services, and infrastructure
- 100% follow through on all communication and tasks
- The ability to deliver results with ambiguous requirements or inputs


#### Compa Groups

General definitions for compa groups can be found on the [GitLab Global Compensation handbook page](/handbook/total-rewards/compensation/#compa-ratio). Below is a more targeted definition of what it means to be in a compa group within each level of security compliance analyst roles.

Within the responsibilities and requirements for this level, the differentiation between compa groups is:
- Learning: New to this level with regular input and guidance needed to accomplish responsibilities.
- Growing:  The ability to operate independently on the vast majority of day-to-day tasks with more guidance needed on new programs and initiatives.
- Thriving: The ability to operate independently on the programatic level with strong cross-team communication. This is the compa group that most security analysts will spend the most time in within a particular role. This compa group involves regularly operating outside of your comfort zone, continually seeking out new work, and helping to distribute work across the team to ensure team-level OKR's are met. Individual OKR expectations are regularly met.
- Expert: Guidance on tasks and projects is rarely needed and there is very little left to learn within the responsibilities and requirements of this role. All work is performed transparently and with a sense of urgency. OKR expectations are regularly exceeded and data exists to support expertise in all areas of responsibilities and requirements.

### Staff Security Analyst

The number of Staff Security Analysts are very limited and it is unlikely that GitLab would ever have more than one Staff Security Analyst at a time given the level of expertise involved with achieving this level of expertise in this field.

#### Responsibilities
- All responsibilities for a Senior Security Analyst; plus:
- Create open-source security compliance programs that deliver value to the GitLab community
- Mentor other Security Compliance Analysts and improve quality and quantity of the team's output
- Program-level leadership across teams outside of Security
- Anticipate external audit requirements in advance
- Predict future industry trends to keep GitLab on the bleeding edge of Security Compliance
- Regularly present at conferences
- Build the GitLab Security Compliance brand


#### Requirements
- All requirements for a Senior Security Analyst; plus:
- Proven ability to create new programs and deliver successful results
- Proven experience building compliance programs from the ground-up
- Proven experience with successful first-time external audits
- Detailed and comprehensive knowledge of all GitLab tools, services, and infrastructure

#### Compa Groups

General definitions for compa groups can be found on the [GitLab Global Compensation handbook page](/handbook/total-rewards/compensation/#compa-ratio). Below is a more targeted definition of what it means to be in a compa group within each level of security compliance analyst roles.

Within the responsibilities and requirements for this level, the differentiation between compa groups is:
- Learning: New to this level with regular input and guidance needed to accomplish responsibilities.
- Growing:  The ability to operate independently on the vast majority of day-to-day tasks with more guidance needed on new programs and initiatives.
- Thriving: The ability to operate independently on the programatic level with strong cross-team communication. This is the compa group that most security analysts will spend the most time in within a particular role. This compa group involves regularly operating outside of your comfort zone, continually seeking out new work, and helping to distribute work across the team to ensure team-level OKR's are met. Individual OKR expectations are regularly met.
- Expert: Guidance on tasks and projects is rarely needed and there is very little left to learn within the responsibilities and requirements of this role. All work is performed transparently and with a sense of urgency. OKR expectations are regularly exceeded and data exists to support expertise in all areas of responsibilities and requirements. An expert-level Staff Security Analyst is in the top 1% of the industry and has almost nothing left to learn about the job or industry.

## External Communications

### Senior External Communications Analyst

The External Communications Team leads customer advocacy, engagement and communications in support of GitLab Security Team programs. Initiatives for this specialty include:

- Increase engagement with the hacker community, including our public bug bounty program.
- Build and manage a Security blogging program.
- Develop social media content and campaigns, in collaboration with GitLab social media manager.
- Manage security alert email notifications.
- Collaborate with corporate marketing, PR, Community Advocates and Technical Evangelism teams to help identify opportunities for the Security Team to increase industry recognition and thought leadership position.

## Security Operations

[Security Operations](/handbook/engineering/security/#security-operations) is responsible for the proactive security measures to protect GitLab the company, GitLab the product, and GitLab.com, as well as detecting and responding to security incidents.  The Security Analysts in Security Operations play a vital role in identifying and responding to incidents, and using the resulting knowledge and experience to help build automated methods of remediating these issues in the future.

### Security Analyst

#### Responsibilities

- Respond and assist with security requests and incidents submitted by GitLab team-members
- Review logging, alerting, and audit sources to identify potential security incidents
- Act on security incidents identified through monitoring and alerting sources
- Contribute to the creation and upkeep of runbooks to handle security incidents
- Work closely with the Security Operations Engineers to improve incident alertings and automated remediation

### Senior Security Analyst

#### Responsibilities

In addition to the responsibilities of a Security Analyst in Security Operations:

* Leverages security expertise in at least one specialty area
* Triage and act on escalated security incidents independently
* Conduct incident RCA's and propose security improvements to prevent or minimize future incidents
* Screen security candidates during the hiring process
* Mentor Security Analyst to improve technical and procedural skills

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with Security Engineer
- Candidates will then be invited to schedule an interview with Director of Security
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Performance Indicators

Security Analysts have the following job-family performance indicators.

### External Communications
* [HackerOne Outreach and Engagement](/handbook/engineering/security/performance-indicators/#hackerone-outreach-and-engagement)
